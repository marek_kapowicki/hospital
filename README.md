# Guidance

We wouldn't ask anyone to do a coding puzzle that we haven't done ourselves, so the people looking at your code understand the problem we're asking to be solved.
If you want to impress us, show that you have considered the following:

* Any indicator of design (DDD, or design patterns) would make us smile
* Are you familiar with Java 8? The Java 8 features are widespread at EdgeLab, but their usage must be justified
* We also consider the extensibility of the code produced. Well factored code should be relatively easily extended. Although we would caution against speculative design
* Check the code for further information

Thank you and have fun!

# Result
When you feel pleased with your solution:

1. Clean the project
2. Zip the project's folder and send it back to us

We're looking forward to reading your code :)