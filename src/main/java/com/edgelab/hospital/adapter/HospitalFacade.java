package com.edgelab.hospital.adapter;

import com.edgelab.hospital.domain.disease.Patient;
import com.edgelab.hospital.domain.disease.medicine.Medicine;
import io.vavr.control.Try;

import java.util.List;

import static com.edgelab.hospital.domain.Hospital.hospital;


public class HospitalFacade {

    public static HospitalFacade hospitalFacade() {
        return new HospitalFacade(new MedicineConverter(), new PatientConverter());
    }

    private final MedicineConverter medicineConverter;
    private final PatientConverter patientConverter;

    private HospitalFacade(MedicineConverter medicineConverter, PatientConverter patientConverter) {
        this.medicineConverter = medicineConverter;
        this.patientConverter = patientConverter;
    }

    public String tryProcess(List<String> patientsString, List<String> drugsString) {
        return Try.of(() -> process(patientsString, drugsString))
                .getOrElseGet(Throwable::getMessage);
    }

    private String process(List<String> patientsString, List<String> drugsString) {
        List<Patient> patients = patientConverter.toPatients(patientsString);
        List<Medicine> medicines = medicineConverter.toMedicines(drugsString);
        return hospital(patients, medicines).process();
    }


}
