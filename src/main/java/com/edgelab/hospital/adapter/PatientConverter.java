package com.edgelab.hospital.adapter;

import com.edgelab.hospital.domain.disease.Patient;
import com.edgelab.hospital.domain.disease.PatientFactory;

import java.util.List;

import static java.util.stream.Collectors.toList;

class PatientConverter {
    List<Patient> toPatients(List<String> patients) {
        return patients.stream()
                .map(PatientFactory::build)
                .collect(toList());
    }

}
