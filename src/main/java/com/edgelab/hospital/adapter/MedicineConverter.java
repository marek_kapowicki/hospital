package com.edgelab.hospital.adapter;

import com.edgelab.hospital.domain.disease.medicine.Medicine;

import java.util.List;

import static com.edgelab.hospital.domain.disease.medicine.Medicines.TO_MEDICINE;
import static java.util.stream.Collectors.toList;

class MedicineConverter {
    List<Medicine> toMedicines(List<String> medicines) {
        return medicines.stream()
                .map(TO_MEDICINE)
                .collect(toList());
    }
}
