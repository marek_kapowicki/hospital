package com.edgelab.hospital.infrastracture.probability;

import com.google.common.annotations.VisibleForTesting;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

public class ConditionalInvoker<T> {
    private final Predicate<T> predicate;

    @VisibleForTesting
    public static <E> ConditionalInvoker<E> alwaysTrue() {
        return new ConditionalInvoker<>(p -> true);
    }

    @VisibleForTesting
    public static <E> ConditionalInvoker<E> alwaysFalse() {
        return new ConditionalInvoker<>(p -> false);
    }

    public static <E> ConditionalInvoker<E> withProbabilityFactor(int factor) {
        checkArgument(factor >= 1, "factor must be grater then 1");
        Predicate<E> probabilityPredicate = ConditionalProbabilityChecker.withProbabilityFactor(factor).toPrecondition();
        return new ConditionalInvoker<>(probabilityPredicate);
    }


    private ConditionalInvoker(Predicate<T> predicate) {
        this.predicate = predicate;
    }

    public Optional<T> invoke(Function<? super T, ? extends T> function, T arg) {
        if (predicate.test(arg)) {
            return ofNullable(function.apply(arg));
        }
        return empty();
    }
}
