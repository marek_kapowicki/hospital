package com.edgelab.hospital.infrastracture.probability;

import org.apache.commons.lang3.RandomUtils;

import java.util.function.Predicate;


class ConditionalProbabilityChecker {

    private final int probabilityFactor;

    static ConditionalProbabilityChecker withProbabilityFactor(int factor) {
        return new ConditionalProbabilityChecker(factor);
    }

    private ConditionalProbabilityChecker(int probabilityFactor) {

        this.probabilityFactor = probabilityFactor;
    }

    <T> Predicate<T> toPrecondition() {
        return p -> RandomUtils.nextInt(0, probabilityFactor) == 0;
    }

}
