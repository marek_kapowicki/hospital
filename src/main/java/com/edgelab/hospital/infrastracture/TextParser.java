package com.edgelab.hospital.infrastracture;

import com.google.common.base.Splitter;

import java.util.List;


public class TextParser {
    private final String separator;

    public static TextParser parser() {
        return new TextParser(",");
    }

    private TextParser(String separator) {
        this.separator = separator;
    }

    public List<String> parse(String input) {

        return Splitter.on(separator)
                .omitEmptyStrings()
                .splitToList(input);
    }
}
