package com.edgelab.hospital.domain;

import com.edgelab.hospital.domain.disease.Patient;
import com.edgelab.hospital.domain.disease.medicine.Medicine;
import com.edgelab.hospital.domain.report.ReportGenerator;
import com.google.common.annotations.VisibleForTesting;

import java.util.List;
import java.util.function.Consumer;

import static java.util.stream.Collectors.toList;

public class Hospital {
    private final List<Patient> patients;
    private final List<Medicine> medicines;
    private final ReportGenerator reportGeneratorGenerator;

    public static Hospital hospital(List<Patient> patients, List<Medicine> medicines) {
        ReportGenerator reportGeneratorGenerator = ReportGenerator.of();
        return new Hospital(patients, medicines, reportGeneratorGenerator);
    }

    private Hospital(List<Patient> patients, List<Medicine> medicines, ReportGenerator reportGeneratorGenerator) {
        this.patients = patients;
        this.medicines = medicines;
        this.reportGeneratorGenerator = reportGeneratorGenerator;
    }

    public String process() {
        return process(this::processPatient);
    }

    @VisibleForTesting
    /**
     * available only for tests
     * no random events like spagetti monster
     * flickering tests are pure evil!!!!
     */
    String processForTest() {
        return process(this::processPatientForTest);
    }

    private String process(Consumer<Patient> patientConsumer) {
        patients.forEach(patientConsumer);
        return report();
    }

    private void processPatient(Patient patient) {
        processPatient(patient, Patient::process);
    }

    private void processPatientForTest(Patient patient) {
        processPatient(patient, Patient::processForTests);
    }

    private void processPatient(Patient patient, Consumer<Patient> patientConsumer) {
        patient.takeAll(medicines);
        patientConsumer.accept(patient);
    }

    private String report() {
        return reportGeneratorGenerator.generate(
                patients.stream()
                        .map(Patient::getHealthStatus).collect(toList()));
    }

}
