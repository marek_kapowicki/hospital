package com.edgelab.hospital.domain.report;

import com.edgelab.hospital.domain.disease.HealthStatus;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public final class ReportGenerator {

    private List<HealthStatus> healthStatusesInOrder;

    private ReportGenerator(final List<HealthStatus> healthStatuses) {
        healthStatusesInOrder = healthStatusesInOrder(healthStatuses);
    }

    public static ReportGenerator of(final List<HealthStatus> healthStatuses) {
        checkArgument(healthStatuses != null);
        return new ReportGenerator(healthStatuses);
    }

    public static ReportGenerator of() {
        return of(HealthStatus.all());
    }

    public String generate(List<HealthStatus> patients) {
        final Map<HealthStatus, Long> patientsGroup = patients.stream()
                .collect(groupingBy(Function.identity(), counting()));
        return generate(patientsGroup);
    }

    private String generate(Map<HealthStatus, Long> patientsGroup) {
        return healthStatusesInOrder.stream()
                .map(it -> it.toString() + ":" + patientsGroup.getOrDefault(it, 0L))
                .collect(Collectors.joining(","));
    }

    private List<HealthStatus> healthStatusesInOrder(List<HealthStatus> result) {
        result.add(HealthStatus.DEAD);
        return result.stream().distinct().collect(toList());
    }
}
