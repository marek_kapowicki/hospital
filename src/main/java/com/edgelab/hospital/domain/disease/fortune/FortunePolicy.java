package com.edgelab.hospital.domain.disease.fortune;

import com.edgelab.hospital.domain.disease.HealthStatus;
import com.google.common.annotations.VisibleForTesting;

import java.util.Optional;

public interface FortunePolicy {
    Optional<HealthStatus> tryApply(HealthStatus healthStatus);

    @VisibleForTesting
    static FortunePolicy alwaysFalsePolicy() {
        return healthStatus -> Optional.empty();
    }

    static FortunePolicy spagettiMonsterPolicy() {
        return SpagettiMonsterPolicy.newInstance();
    }

}
