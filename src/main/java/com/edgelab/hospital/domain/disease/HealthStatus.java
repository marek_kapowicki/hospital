package com.edgelab.hospital.domain.disease;

import com.google.common.collect.Lists;

import java.util.List;

public enum HealthStatus {
    FEVER("F"), HEALTHY("H"), DIABETES("D"),
    TUBERCULOSIS("T"), DEAD("X");
    private String value;

    HealthStatus(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static List<HealthStatus> all() {
        return Lists.newArrayList(values());
    }


    public static HealthStatus toHealthStatus(String status) {
        return all().stream()
                .filter(it -> status.equals(it.value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("not defined healthStatus: " + status));
    }

}
