package com.edgelab.hospital.domain.disease.medicine;

import com.edgelab.hospital.domain.disease.HealthStatus;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.function.Predicate;

class Result {

    private final HealthStatus prediction;
    private final List<HealthStatus> patientsStatuses;
    private final MedicineName mixing;

    private Result(HealthStatus prediction, List<HealthStatus> heslthStatuses, MedicineName mixing) {
        this.prediction = prediction;
        this.patientsStatuses = heslthStatuses;
        this.mixing = mixing;
    }

    HealthStatus getPrediction() {
        return prediction;
    }

    Predicate<HealthStatus> applicableFor() {
        return patientsStatuses::contains;
    }

    Predicate<MedicineName> riskyWith() {
        return mixing::equals;
    }

    static class Builder {
        private HealthStatus prediction;
        private List<HealthStatus> patientsStatuses = Lists.newArrayList();
        private MedicineName mixing;

        private Builder(HealthStatus prediction) {
            this.prediction = prediction;
        }

        static Builder result(HealthStatus prediction) {
            return new Builder(prediction);
        }

        Builder forPatientStatuses(List<HealthStatus> patients) {
            this.patientsStatuses = patients;
            return this;
        }

        Builder mixedWith(MedicineName medicine) {
            this.mixing = medicine;
            return this;
        }

        Result build() {
            return new Result(prediction, patientsStatuses, mixing);
        }
    }
}
