package com.edgelab.hospital.domain.disease;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

import static com.edgelab.hospital.domain.disease.HealthStatus.DEAD;
import static com.edgelab.hospital.domain.disease.HealthStatus.DIABETES;
import static com.edgelab.hospital.domain.disease.HealthStatus.FEVER;
import static com.edgelab.hospital.domain.disease.HealthStatus.HEALTHY;
import static com.edgelab.hospital.domain.disease.HealthStatus.TUBERCULOSIS;

public final class PatientFactory {
    private static final Map<HealthStatus, HealthStatus> HEALTH_STATUSES =
            new ImmutableMap.Builder<HealthStatus, HealthStatus>()
                    .put(DEAD, DEAD)
                    .put(DIABETES, DEAD)
                    .put(FEVER, FEVER)
                    .put(TUBERCULOSIS, TUBERCULOSIS)
                    .put(HEALTHY, HEALTHY).build();

    private PatientFactory() {
    }

    public static Patient build(String status) {
        HealthStatus healthStatus = HealthStatus.toHealthStatus(status);
        return build(healthStatus);
    }

    public static Patient build(HealthStatus healthStatus) {
        return Patient.of(healthStatus, HEALTH_STATUSES.get(healthStatus));
    }
}