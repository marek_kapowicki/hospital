package com.edgelab.hospital.domain.disease.medicine;

import com.edgelab.hospital.domain.disease.HealthStatus;

import java.util.List;

import static com.edgelab.hospital.domain.disease.HealthStatus.DEAD;

public class MedicineApplierPolicy {
    private final HealthStatus healthStatus;
    private final Medicine medicine;

    private MedicineApplierPolicy(HealthStatus healthStatus, Medicine medicine) {
        this.healthStatus = healthStatus;
        this.medicine = medicine;
    }

    public static MedicineApplierPolicy of(HealthStatus healthStatus, Medicine medicine) {
        return new MedicineApplierPolicy(healthStatus, medicine);
    }

    public HealthStatus choosePrediction(final HealthStatus patientPrediction,
                                         final List<Medicine> takenMedicines) {
        if (healthStatus == DEAD) {
            return DEAD;
        }

        if (medicine.riskyFor().test(healthStatus)
                && medicine.riskyWith().test(takenMedicines)) {
            return medicine.getSideEffect();
        }
        if (medicine.usefulFor().test(healthStatus)) {
            return medicine.getExpectedResult();
        }
        return patientPrediction;
    }

}
