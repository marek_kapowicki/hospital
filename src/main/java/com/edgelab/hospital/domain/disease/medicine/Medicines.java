package com.edgelab.hospital.domain.disease.medicine;

import com.edgelab.hospital.domain.disease.HealthStatus;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.function.Function;

import static com.edgelab.hospital.domain.disease.HealthStatus.DEAD;
import static com.edgelab.hospital.domain.disease.HealthStatus.DIABETES;
import static com.edgelab.hospital.domain.disease.HealthStatus.FEVER;
import static com.edgelab.hospital.domain.disease.HealthStatus.HEALTHY;
import static com.edgelab.hospital.domain.disease.HealthStatus.TUBERCULOSIS;
import static com.edgelab.hospital.domain.disease.medicine.Medicine.Builder.builder;
import static com.edgelab.hospital.domain.disease.medicine.MedicineName.ANTIBIOTIC;
import static com.edgelab.hospital.domain.disease.medicine.MedicineName.ASPIRIN;
import static com.edgelab.hospital.domain.disease.medicine.MedicineName.INSULIN;
import static com.edgelab.hospital.domain.disease.medicine.MedicineName.PARACETAMOL;
import static com.edgelab.hospital.domain.disease.medicine.Result.Builder.result;
import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Collections.singletonList;
import static java.util.Optional.ofNullable;


public class Medicines {
    public static final Medicine paracetamol =
            builder(PARACETAMOL,
                    result(HEALTHY)
                            .forPatientStatuses(singletonList(FEVER)).build())
                    .sideEffects(result(DEAD)
                            .forPatientStatuses(HealthStatus.all())
                            .mixedWith(ASPIRIN).build())

                    .build();
    public static final Medicine aspirin =
            builder(ASPIRIN,
                    result(HEALTHY)
                            .forPatientStatuses(singletonList(FEVER)).build())
                    .sideEffects(result(DEAD)
                            .forPatientStatuses(HealthStatus.all())
                            .mixedWith(PARACETAMOL).build())
                    .build();

    public static final Medicine antibiotic =
            builder(ANTIBIOTIC,
                    result(HEALTHY)
                            .forPatientStatuses(singletonList(TUBERCULOSIS)).build())
                    .sideEffects(result(FEVER)
                            .forPatientStatuses(singletonList(HEALTHY))
                            .mixedWith(INSULIN).build())
                    .build();

    public static final Medicine insulin =
            builder(INSULIN,
                    result(HealthStatus.DIABETES)
                            .forPatientStatuses(singletonList(DIABETES)).build())
                    .sideEffects(result(FEVER)
                            .forPatientStatuses(singletonList(HEALTHY))
                            .mixedWith(ANTIBIOTIC).build())
                    .build();

    public static final Function<String, Medicine> TO_MEDICINE = Medicines::toMedicine;

    public static final Medicine toMedicine(String shortName) {
        checkArgument(StringUtils.isNotBlank(shortName), "the name of medicine is empty");
        return ofNullable(MEDICINE_MAP.get(shortName))
                .orElseThrow(() -> new IllegalArgumentException("not defined medicine: " + shortName));
    }

    private static final Map<String, Medicine> MEDICINE_MAP =
            new ImmutableMap.Builder<String, Medicine>()
                    .put("As", aspirin)
                    .put("An", antibiotic)
                    .put("I", insulin)
                    .put("P", paracetamol).build();


    private Medicines() {
    }
}
