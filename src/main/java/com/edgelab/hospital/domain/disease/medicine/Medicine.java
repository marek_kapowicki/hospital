package com.edgelab.hospital.domain.disease.medicine;

import com.edgelab.hospital.domain.disease.HealthStatus;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

public class Medicine {
    private final MedicineName name;
    private final Result expected;
    private final Result sideEffects;

    private Medicine(MedicineName name, Result expected, Result sideEffects) {
        this.name = name;
        this.expected = expected;
        this.sideEffects = sideEffects;
    }

    HealthStatus getExpectedResult() {
        return expected.getPrediction();
    }

    private Optional<Result> sideEffects() {
        return Optional.ofNullable(sideEffects);
    }

    HealthStatus getSideEffect() {
        return sideEffects()
                .map(Result::getPrediction)
                .orElseThrow(() -> new IllegalStateException("side effect should be here"));
    }

    Predicate<HealthStatus> usefulFor() {
        return expected.applicableFor();
    }

    Predicate<HealthStatus> riskyFor() {
        return sideEffects()
                .map(Result::applicableFor)
                .orElse(it -> false);
    }

    Predicate<List<Medicine>> riskyWith() {
        return history ->
                history.stream()
                        .map(Medicine::getName)
                        .anyMatch(sideEffects().get().riskyWith());
    }

    private MedicineName getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Medicine medicine = (Medicine) o;
        return Objects.equals(name, medicine.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name.toString();
    }

    static class Builder {
        private MedicineName name;
        private Result proper;
        private Result sideEffects;

        private Builder(MedicineName name, Result proper) {
            this.name = name;
            this.proper = proper;
        }

        static Builder builder(MedicineName name, Result proper) {
            return new Builder(name, proper);
        }

        Builder sideEffects(Result sideEffects) {
            this.sideEffects = sideEffects;
            return this;
        }

        Medicine build() {
            return new Medicine(name, proper, sideEffects);
        }
    }
}