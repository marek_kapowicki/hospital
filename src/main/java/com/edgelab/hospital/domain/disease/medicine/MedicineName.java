package com.edgelab.hospital.domain.disease.medicine;

enum MedicineName {
    PARACETAMOL, ASPIRIN, ANTIBIOTIC, INSULIN;
}
