package com.edgelab.hospital.domain.disease.fortune;

import com.edgelab.hospital.domain.disease.HealthStatus;
import com.edgelab.hospital.infrastracture.probability.ConditionalInvoker;

import java.util.Optional;

import static com.edgelab.hospital.domain.disease.HealthStatus.DEAD;
import static com.edgelab.hospital.domain.disease.HealthStatus.HEALTHY;

class SpagettiMonsterPolicy implements FortunePolicy {
    private static final int PROBABILITY_FACTOR = 1_000_000; //it should be the spring boot property
    private final ConditionalInvoker<HealthStatus> conditionalInvoker;

    static SpagettiMonsterPolicy newInstance() {
        return new SpagettiMonsterPolicy(ConditionalInvoker.withProbabilityFactor(PROBABILITY_FACTOR));
    }

    private SpagettiMonsterPolicy(ConditionalInvoker<HealthStatus> conditionalInvoker) {
        this.conditionalInvoker = conditionalInvoker;
    }

    public Optional<HealthStatus> tryApply(HealthStatus healthStatus) {
        return conditionalInvoker.invoke(SpagettiMonsterPolicy::noodlyPowerFunction, healthStatus);
    }

    private static HealthStatus noodlyPowerFunction(HealthStatus healthStatus) {
        if (healthStatus == DEAD) {
            return HEALTHY;
        }
        return healthStatus;
    }
}
