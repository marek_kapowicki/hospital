package com.edgelab.hospital.domain.disease;

import com.edgelab.hospital.domain.disease.fortune.FortunePolicy;
import com.edgelab.hospital.domain.disease.medicine.Medicine;
import com.edgelab.hospital.domain.disease.medicine.MedicineApplierPolicy;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.util.List;

import static com.edgelab.hospital.domain.disease.fortune.FortunePolicy.spagettiMonsterPolicy;

public class Patient {
    private HealthStatus healthStatus;
    private final List<Medicine> takenMedicines = Lists.newArrayList();
    private HealthStatus prediction;

    private Patient(HealthStatus healthStatus, HealthStatus prediction) {
        this.healthStatus = healthStatus;
        this.prediction = prediction;
    }

    static Patient of(HealthStatus healthStatus, HealthStatus prediction) {
        return new Patient(healthStatus, prediction);
    }

    public void process() {
        process(spagettiMonsterPolicy());
    }

    /**
     * we don't want to have the random factor in tests
     * for test the random events are always switched off
     */
    @VisibleForTesting
    public void processForTests() {
        process(FortunePolicy.alwaysFalsePolicy());
    }

    private void process(FortunePolicy fortunePolicy) {
        this.healthStatus = fortunePolicy.tryApply(healthStatus).orElseGet(() -> prediction);
    }

    public void takeAll(List<Medicine> medicines) {
        medicines.forEach(this::take);
    }

    public void take(Medicine medicine) {
        this.prediction = MedicineApplierPolicy.of(healthStatus, medicine).choosePrediction(prediction, getTakenMedicines());
        takenMedicines.add(medicine);
    }

    private List<Medicine> getTakenMedicines() {
        return ImmutableList.copyOf(takenMedicines);
    }

    public HealthStatus getHealthStatus() {
        return healthStatus;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "healthStatus='" + healthStatus + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Patient patient = (Patient) o;
        return Objects.equal(healthStatus, patient.healthStatus);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(healthStatus);
    }
}
