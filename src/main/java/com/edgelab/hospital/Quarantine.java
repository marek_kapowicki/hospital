package com.edgelab.hospital;

import com.edgelab.hospital.domain.disease.Patient;
import com.edgelab.hospital.domain.disease.PatientFactory;
import com.edgelab.hospital.domain.disease.medicine.Medicines;
import com.edgelab.hospital.domain.report.ReportGenerator;
import com.google.common.base.Splitter;

import java.util.List;

import static com.edgelab.hospital.domain.disease.medicine.Medicines.antibiotic;
import static com.edgelab.hospital.domain.disease.medicine.Medicines.aspirin;
import static com.edgelab.hospital.domain.disease.medicine.Medicines.paracetamol;
import static java.util.stream.Collectors.toList;

final class Quarantine {
    private final ReportGenerator reportGeneratorGenerator;
    private List<Patient> patients;

    Quarantine(String subjects) {
        List<String> patientStrings = Splitter.on(',')
                .omitEmptyStrings()
                .splitToList(subjects);

        this.patients =
                patientStrings.stream()
                        .map(PatientFactory::build)
                        .collect(toList());
        reportGeneratorGenerator = ReportGenerator.of(
                patients.stream()
                        .map(Patient::getHealthStatus).collect(toList()));
    }

    void aspirin() {
        patients.forEach(p -> p.take(aspirin));
    }

    void antibiotic() {
        patients.forEach(p -> p.take(antibiotic));
    }

    void insulin() {
        patients.forEach(p -> p.take(Medicines.insulin));
    }

    void paracetamol() {
        patients.forEach(p -> p.take(paracetamol));
    }

    void wait40Days() {
        patients.forEach(Patient::processForTests);
    }

    String report() {
        return reportGeneratorGenerator.generate(
                patients.stream()
                        .map(Patient::getHealthStatus).collect(toList()));
    }

}