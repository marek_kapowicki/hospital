package com.edgelab.hospital;


import com.edgelab.hospital.adapter.HospitalFacade;
import com.edgelab.hospital.infrastracture.TextParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Application {

    private static final Logger logger = LogManager.getLogger(Application.class);
    private static final TextParser textParser = TextParser.parser();

    public static void main(String[] args) {
        logger.info(HospitalFacade.hospitalFacade().tryProcess(patients(args), drugs(args)));

    }

    private static List<String> patients(String[] args) {
        return textParser.parse(parseArgs(args, 0));
    }

    private static List<String> drugs(String[] args) {
        return textParser.parse(parseArgs(args, 1));
    }

    private static String parseArgs(String[] args, int no) {
        if (args == null || args.length < no + 1) {
            return "";
        }
        return args[no];
    }
}
