package com.edgelab.hospital.domain

import spock.lang.Specification

import static com.edgelab.hospital.domain.disease.HealthStatus.DIABETES
import static com.edgelab.hospital.domain.disease.HealthStatus.FEVER
import static com.edgelab.hospital.domain.disease.medicine.Medicines.paracetamol

/**
 * specification by example - samples from the feature description
 */
class HospitalSpec extends Specification {
    def 'diabetic patients die without insulin'() {
        given:
            Hospital hospital = Hospital.hospital([com.edgelab.hospital.domain.Patients.create(DIABETES), com.edgelab.hospital.domain.Patients.create(DIABETES)], [])
        when:
            String result = hospital.processForTest();
        then:
            result == 'F:0,H:0,D:0,T:0,X:2'
    }

    def 'paracetamol cures fever'() {
        given:
            Hospital hospital = Hospital.hospital([com.edgelab.hospital.domain.Patients.create(FEVER)], [paracetamol])
        when:
            String result = hospital.processForTest();
        then:
            result == 'F:0,H:1,D:0,T:0,X:0'
    }
}
