package com.edgelab.hospital.domain

import com.edgelab.hospital.domain.disease.HealthStatus
import com.edgelab.hospital.domain.disease.Patient
import com.edgelab.hospital.domain.disease.PatientFactory

class Patients {
    static Patient create(HealthStatus status) {
        return PatientFactory.build(status)
    }
}
