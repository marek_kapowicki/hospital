package com.edgelab.hospital.domain

import com.edgelab.hospital.domain.disease.Patient
import spock.lang.Specification
import static com.edgelab.hospital.domain.disease.HealthStatus.*
import static com.edgelab.hospital.domain.disease.medicine.Medicines.*

class QurantineSpec extends Specification {

    def "provide drugs: #medicines to all patients"() {
        given:
            List<Patient> patients = [Patients.create(FEVER), Patients.create(HEALTHY), Patients.create(DIABETES), Patients.create(DIABETES),
                                      Patients.create(DIABETES), Patients.create(HEALTHY), Patients.create(TUBERCULOSIS)]
        and:
            Hospital hospital = Hospital.hospital(patients, medicines)
        when:
            String result = hospital.processForTest()
        then:
            result == expecedResult
        where:
            medicines              || expecedResult
            []                     || 'F:1,H:2,D:0,T:1,X:3'
            [aspirin]              || 'F:0,H:3,D:0,T:1,X:3'
            [antibiotic]           || 'F:1,H:3,D:0,T:0,X:3'
            [insulin]              || 'F:1,H:2,D:3,T:1,X:0'
            [antibiotic, insulin]  || 'F:3,H:1,D:3,T:0,X:0'
            [insulin, antibiotic]  || 'F:3,H:1,D:3,T:0,X:0'
            [paracetamol]          || 'F:0,H:3,D:0,T:1,X:3'
            [paracetamol, aspirin] || 'F:0,H:0,D:0,T:0,X:7'
            [aspirin, paracetamol] || 'F:0,H:0,D:0,T:0,X:7'
    }
}
